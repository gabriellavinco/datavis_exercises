# Data Visualization

This repository contains the website and code used for the exercises of the Data
Visualisation course at UHasselt and KU Leuven. For instructions on how to get started
see [https://datavis-exercises.vercel.app/](https://datavis-exercises.vercel.app/).

## Getting started

Check our [instructions](https://datavis-exercises.vercel.app/instructions/initial_setup) here.

In short, if you know what you are doing:

- Fork this repository.
- Import your fork to Vercel, use `[studentnumber]-datavis-exercises` as URL.
- Clone the repository and install dependencies with `npm install`.
- Register this repository as remote upstream.
- Add your name and student number in [\_\_layout.svelte](src/routes/__layout.svelte).
- Start a development server, read and perform the exercise for week 0.
- Upload your Vercel and GitLab URL to Blackboard / Toledo.
- Check out the [SvelteKit](https://kit.svelte.dev/docs),
  [Svelte](https://svelte.dev/docs),
  [Bootstrap](https://getbootstrap.com/docs/5.0/getting-started/introduction/)
  docs if interested :)

## The exercises

Check our [instructions](https://datavis-exercises.vercel.app/instructions/working_on_exercises) here.

In short, if you know what you are doing: each week we will release new
instructions on this repository. Rebase your fork with the update and see that
week's instructions. When you are done with the exercises, publish your work by
committing and pushing your changes to your fork. Make sure all your changes
work properly on Vercel and indicate you are finished on Blackboard / Toledo.
